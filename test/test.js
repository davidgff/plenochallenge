const mysql = require('mysql2');
const config = require('../config.json');
var primenumbers = require('../src/erastosthenes');
const chai = require('chai');
var assert = chai.assert;

describe('Mysql', function() {
    var connection = mysql.createConnection({
        host: config.mysql.host,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.database,
    });

    describe('Connection', function() {
      it('Should return no error for mysql connection', (done) => {
        connection.connect(function(err) {
            assert.equal(null, err);
            if(err){
                return;
            }
            
        });
        done();
      });
    });
    describe('Table student', function() {
        describe('Table student exist', function() {
            it('Should return no error for a select limited to 1', (done) => {
                connection.query(
                    'SELECT * FROM student Limit 1',
                    function(err) {
                        assert.equal(null, err);
                        if(err){
                            return;
                        }
                    }
                );
                done();
            });
        });

        describe('Table student is empty', function() {
            it('Should return 0 rows', (done) => {
                connection.query(
                    'SELECT COUNT(*) AS studentCount FROM student',
                    function(err, results) {
                        assert.equal(0, results[0].studentCount,'Table students should be empty to populate, you can run command node drop_table.js');
                        //expect(results[0].studentCount).to.equal(0);
                        if(err){
                            return;
                        }
                    }
                );
                done();
            });
        });
    });
});

describe('Primes', function() {

    describe('Generate AMOUNT primes in config.json', function() {
        it('Should generate array length exactly as AMOUNT: '+config.app.amount, (done) => {
            var primes = [];
            primes = primenumbers.get(config.app.amount);
            assert.equal(config.app.amount, primes.length,'Error getting AMOUNT of prime numbers');
            done();
        });
    });

    describe('Test random index to see if it is prime', function() {
        it('Should be ramdom the number in the index of the array', (done) => {
            var primes = [];
            primes = primenumbers.get(config.app.amount);
            var min = 0;
            var max = Math.floor(config.app.amount);
            var ramdom_number = Math.floor(Math.random() * (max - min + 1)) + min;
            var ramdom_number_is_prime = primenumbers.isPrime(primes[ramdom_number]);
            assert.equal(true, ramdom_number_is_prime,'Error generating the primes - there are numbers in the array that are not prime');
            done();
        });
    });

});
