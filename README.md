**This is a test app for an interview that generates prime numbers and saves them as a student id to a mysql table.**

**It uses the sieve-of-eratosthenes algorithm for better perfomance.**

## Commands
- npm install (install dependencies)
- npm test  (run tests)
- npm start (generate an AMOUNT of prime numbers)
- node drop_table.js (Erase all rows from table student)

The last command is needed because the population of prime numbers is done over an empty table. So if you want to run the app several times, you should this command to empty the table, otherwise the student_id primary key would give an error.

If it is needed to populate a table with data, meaning, adding data, another aproach is needed where the prime function generates the prime from a given number.

##Output of npm start
```
Execution time Primes: 0s 2.484576ms
Execution time Mysql: 0s 10.486726ms
Execution time Total: 0s 13.026529ms

```

## Prerequesites
- Node
- NPM
- Mysql

# Installation

## Step 1:
Here's the script for the database:

```

--
-- Database: `pleno_challenge`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `course_start_date` date NOT NULL,
  `course_end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_professor`
--

CREATE TABLE `course_professor` (
  `course_id` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  `course_professor_salary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_student`
--

CREATE TABLE `course_student` (
  `course_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_student_grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professor`
--

CREATE TABLE `professor` (
  `professor_id` int(11) NOT NULL,
  `professor_name` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Professor Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL,
  `student_name` varchar(1000) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'student name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `course_professor`
--
ALTER TABLE `course_professor`
  ADD KEY `cp_professor_id` (`professor_id`),
  ADD KEY `cp_course_id` (`course_id`);

--
-- Indexes for table `course_student`
--
ALTER TABLE `course_student`
  ADD KEY `cs_course_id` (`course_id`),
  ADD KEY `cs_student_id` (`student_id`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`professor_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `professor`
--
ALTER TABLE `professor`
  MODIFY `professor_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_professor`
--
ALTER TABLE `course_professor`
  ADD CONSTRAINT `cp_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  ADD CONSTRAINT `cp_professor_id` FOREIGN KEY (`professor_id`) REFERENCES `professor` (`professor_id`) ON UPDATE CASCADE;

--
-- Constraints for table `course_student`
--
ALTER TABLE `course_student`
  ADD CONSTRAINT `cs_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  ADD CONSTRAINT `cs_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON UPDATE CASCADE;


```

## Step 2
Clone repository:
```
git clone git@bitbucket.org:davidgff/plenochallenge.git
```

## Step 3
Copy file config.json.COPY into config.json and change the MYSQL variables according to your configuration.

The file looks like this:

```
{
    "mysql": {
        "host": "",
        "user": "",
        "password": "",
        "database": ""
    },
    "app": {
        "amount": 1200
    }
}
```

## Step 4
Run for install the dependencies:
`npm install
`
## Step 5
Run for testing:
`npm test
`

## Step 6
Run for start the app
`npm start
`

- - - -


**Author: David Freitez**