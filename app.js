#!/usr/bin/env node
const config = require('./config.json');
var primenumbers = require('./src/erastosthenes');
var write_to_db = require('./src/write_to_db');

var start_all = process.hrtime();
// Get primes numbers
var start_primes = process.hrtime();
var amount = config.app.amount;
var primes = [];
primes = primenumbers.get(amount);
var end_primes = process.hrtime(start_primes);

// Write prime id's to database
var start_mysql = process.hrtime();
write_to_db.init();

//Method number 2 was faster
write_to_db.write_2(primes);
var end_mysql = process.hrtime(start_mysql);

// Log results
var end_all = process.hrtime(start_all);
console.info('Execution time Primes: %ds %dms', end_primes[0], end_primes[1] / 1000000);
console.info('Execution time Mysql: %ds %dms', end_mysql[0], end_mysql[1] / 1000000);
console.info('Execution time Total: %ds %dms', end_all[0], end_all[1] / 1000000);
