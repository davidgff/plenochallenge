const mysql = require('mysql2');
const config = require('../config.json');

module.exports = {
    init: function(){
        this.connection = mysql.createConnection({
            host: config.mysql.host,
            user: config.mysql.user,
            password: config.mysql.password,
            database: config.mysql.database,
        });

        this.connection.connect(function(err) {
            if(err){
                console.log(err);
                return false;
            } else {
                return true;
            }
        });
    },
    write: function(primes){
        primes.forEach(element => {
            this.connection.query("INSERT INTO student (student_id, student_name) VALUES (" + element + ", 'John Doe')", function(err, rows) {
                if (err)
                    console.log('Error while performing Query.');
            });
        });
        
        this.connection.end();
    },
    write_2: function(primes){
        // The bulk method according to mysql
        let query = "INSERT INTO student ( student_id ) VALUES ?";
        var values=[];

        primes.forEach(element => {
            values.push([element]);
        });

        this.connection.query(query, [values], function(err) {
            if(err)
            console.log('Error while performing Query.');
        });        
        this.connection.end();
    },
    drop_table_student: function(){
        this.connection.query("DELETE FROM student", function(err) {
            if (err)
                console.log(err);
        });
        this.connection.end();
    }
};