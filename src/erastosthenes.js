module.exports = {
    // Implementation of the Sieve of Eratosthenes algorithm 
    // to search for prime numbers
    get: function(maxCount){
        // Get 'maxCount' amount of prime numbers using the algorithm
        var primes = [];
        var n = this.calculate_top_boundary(maxCount);
        primes = this.sieve(n);
        
        // As the algorithm uses an aproximation of n, we will remove
        // the spare ones.
        primes = primes.slice(0,maxCount);

        return primes;
    },
    calculate_top_boundary: function(n){
        // Looking for an aproximation to be used in erastosthenes 
        // to build the array with n values needed

        var N = n * (Math.log(n) + Math.log(Math.log(n)));
        return N;
    },
    sieve: function(n){
        // Algorithm to search first prime numbers under n.

        var array = [], upperLimit = Math.sqrt(n), output = [];

        // Make an array from 2 to (n - 1)
        for (var i = 0; i < n; i++) {
            array.push(true);
        }

        // Remove multiples of primes starting from 2, 3, 5,...
        for (var i = 2; i <= upperLimit; i++) {
            if (array[i]) {
                for (var j = i * i; j < n; j += i) {
                    array[j] = false;
                }
            }
        }

        // All array[i] set to true are primes
        for (var i = 2; i < n; i++) {
            if(array[i]) {
                output.push(i);
            }
        }

        return output;
    },
    isPrime: function(num){
        for(var i = 2; i < num; i++)
          if(num % i === 0) return false;
        return num > 1;
    }
};
